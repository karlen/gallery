<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gallery');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hOgO<;zzZ4*qd,Cv=_1{9vqj{mC-wJ+!@GBYLk_{6_>RU+CYb9/%;ILZs>j:#7Ng');
define('SECURE_AUTH_KEY',  'c]}lGY[4iETamDdc>,Gia+Qhe23GoCV^W7-fdgdZ8/UI;M#+9z$Hu[X029[~{L`U');
define('LOGGED_IN_KEY',    '$uqZ(Kn(u2]dd[;By_NgK$*y:;IA;E.m|f0R>pTJ%1oH.q}}PB<3314^1TxH489u');
define('NONCE_KEY',        'rh-[PXxXUQ9XK-E-b:h%CJaE[b-vM5#,heV/Gr@AaiY$8@*.h3d1+D`(GQP*)?<G');
define('AUTH_SALT',        'KXt2]| CM>`SKAUqCqk+G-o#vxX>ybp~ !~u|vE%|d[HzF[OYb.-4F#D,w-xM^Qd');
define('SECURE_AUTH_SALT', '|0U>Zn,`cOI|L<)lw%z)*4Bj6NO5.h4PE:4Floj&d7z+N1n/| cauA^Uu 4iD SL');
define('LOGGED_IN_SALT',   '^*@33:u}/FU KX-+q2<98&y?@~+G%e7ijcb5x*eML#p^^2eEO#/E=SOrr||-eFMl');
define('NONCE_SALT',       '*1Nba|^K[%dOw>t?=SFi3*J`~wap]4C79ER:.,-CEEPT+F.;*?r9C<eg$>E n`Ap');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
