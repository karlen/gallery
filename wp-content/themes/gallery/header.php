<?php
/**
 * @package WordPress
 * @subpackage Gallery
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html id="ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <?php
    $sliderImages = get_field('slider_images', 'option');
    global $post;
    $meta_description = '';
    if ( is_front_page() ) {
        $meta_description = "Галерея картин Елены Смирновой в Санкт-Петербурге находится в ТК Рио в Купчино. Вы можете приехать в салон посмотреть произведения исскуства или купить картины в интернет-магазине.";
        $title = $meta_description;
        ?>
        <meta property="og:type" content="website">
        <meta property="og:title" content="Es-Salon">
        <meta property="og:url" content="<?php echo( home_url( '/' ) ); ?>">
        <meta property="og:image" content="<?php echo $sliderImages[0]['url']; ?>">
        <meta property="og:description" content="<?php echo( esc_attr( $meta_description ) ); ?>">
        <?php
    }else if(is_search()){
        $meta_description = "Галерея картин Елены Смирновой в Санкт-Петербурге находится в ТК Рио в Купчино. Вы можете приехать в салон посмотреть произведения исскуства или купить картины в интернет-магазине.";
        $title = 'Поиск '.$meta_description;
        ?>
        <meta property="og:type" content="website">
        <meta property="og:title" content="Es-Salon">
        <meta property="og:url" content="<?php echo( home_url( '/' ) ); ?>">
        <meta property="og:image" content="<?php echo $sliderImages[0]['url']; ?>">
        <meta property="og:description" content="<?php echo( esc_attr( $meta_description ) ); ?>">
        <?php
        }else if($post->post_type == 'artists'){
        $meta_description = $post->post_content;
        $title = 'Художник '.$post->post_title.' Галерея картин Елены Смирновой';
        ?>
        <meta property="og:type" content="profile">
        <meta property="og:url" content="<?php echo( get_permalink($post->ID) ); ?>">
        <meta property="og:image" content="<?php echo get_the_post_thumbnail_url($post); ?>">
        <meta property="og:title" content="<?php echo( $post->post_title ); ?>">
        <meta property="og:description" content="<?php  echo( wp_strip_all_tags(esc_attr($post->post_content), true ) ); ?>">
        <?php
    }else if(is_category()){
        $cat_catID = get_cat_id( single_cat_title("",false) );
        $cat_category = get_term_by('id', $cat_catID, 'category');
        $cat_img = json_decode($cat_category->term_thumbnail);
        $meta_description = $cat_category->description;
        $title = 'Категория '.$cat_category->name.' Галерея картин Елены Смирновой';
        ?>
        <meta property="og:type" content="article">
        <meta property="og:url" content="<?php echo( get_category_link($cat_catID) ); ?>">
        <meta property="og:image" content="<?php echo $cat_img->url; ?>">
        <meta property="og:title" content="<?php echo( $cat_category->name ); ?>">
        <meta property="og:description" content="<?php echo( wp_strip_all_tags(esc_attr($cat_category->description ), true ) ); ?>">
        <?php
    }else if(is_tax()){
        $cat_category = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $cat_img = json_decode($cat_category->term_thumbnail);
        $meta_description = $cat_category->description;
        $cat_taxonomy_details = get_taxonomy( $cat_category->taxonomy );
        $title = $cat_taxonomy_details->labels->name.' '.$cat_category->name.' Галерея картин Елены Смирновой';
        ?>
        <meta property="og:type" content="article">
        <meta property="og:url" content="<?php echo( get_term_link($cat_category->term_id) ); ?>">
        <meta property="og:image" content="<?php echo $cat_img->url; ?>">
        <meta property="og:title" content="<?php echo( $cat_category->name ); ?>">
        <meta property="og:description" content="<?php echo( wp_strip_all_tags(esc_attr($cat_category->description ), true ) ); ?>">
        <?php
    }else if($post->post_type == 'post'){
        $meta_description = $post->post_content;
        $title = 'Картина '.$post->post_title.' Галерея картин Елены Смирновой';
        ?>
        <meta property="og:type" content="article">
        <meta property="og:url" content="<?php echo( get_permalink($post->ID) ); ?>">
        <meta property="og:image" content="<?php echo get_the_post_thumbnail_url($post); ?>">
        <meta property="og:title" content="<?php echo( $post->post_title ); ?>">
        <meta property="og:description" content="<?php echo( wp_strip_all_tags(esc_attr($post->post_content), true ) ); ?>">
        <?php
    }else if(is_page()){
        $meta_description = $post->post_content;
        $title = $post->post_title.' Галерея картин Елены Смирновой';
        ?>
        <meta property="og:type" content="article">
        <meta property="og:url" content="<?php echo( get_permalink($post->ID) ); ?>">
        <meta property="og:image" content="<?php echo get_the_post_thumbnail_url($post); ?>">
        <meta property="og:title" content="<?php echo( $post->post_title ); ?>">
        <meta property="og:description" content="<?php echo( wp_strip_all_tags(esc_attr($post->post_content), true ) ); ?>">
        <?php
    }
    ?>
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo( wp_strip_all_tags(esc_attr( $meta_description ), true) ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js" type="text/javascript"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-2.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap-chosen/chosen.jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/nouislider/nouislider.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap-chosen/bootstrap-chosen.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/nouislider/nouislider.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/owl/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" rel="stylesheet">
    <script>
        <?php

            $imagesJsArr = '[';
            if(!empty($sliderImages)){
                    foreach($sliderImages as $sliderImage){
                        $imagesJsArr .= "'".$sliderImage['url']."',";
                    }
            }
            $imagesJsArr = substr($imagesJsArr, 0, -1);
            $imagesJsArr .= ']';


        ?>
        window.sliderImages =<?php echo $imagesJsArr;?>;
    </script>

    <?php wp_head(); ?>
</head>
<?php
$cart = $_COOKIE['cart'];
$cart = empty($cart) ? array() : explode(',',$cart);
$orders = array();
foreach($cart as $order){
    if(!empty($order)){
        $orders[] = (int)$order;
    }
}
?>
<body <?php body_class(); ?>>
<div class="container main_container">
    <div class="row main_header">
        <div class="col-lg-4 col-md-4 col-sm-12 border_bottom">
            <div class="text">Арт салон</div>
            <div class="border"></div>
        </div>
        <a href="/">
            <div class="col-lg-4 col-md-4 col-sm-12 logo">

            </div>
        </a>
        <div class="col-lg-4 col-md-4 col-sm-12 border_bottom">
            <div class="text">Елены Смирновой</div>
            <div class="border"></div>
        </div>
    </div>
    <div class="row main_content">
        <a href="/basket">
            <div class="basket">
                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                <span class="basket_count"><?php echo count($orders); ?></span>
            </div>
        </a>
        <div class="slider prev">
            <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
        </div>
        <div class="slider next">
            <span class="glyphicon  glyphicon-menu-right" aria-hidden="true"></span>
        </div>
        <div class="sidebar col-lg-4 col-md-4 col-sm-12">
            <?php get_sidebar(); ?>
        </div>
        <div class="wrapper col-lg-8 col-md-8 col-sm-12">
            <?php
            get_template_part('components/search');
            ?>
