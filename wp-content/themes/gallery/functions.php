<?php
add_action( 'init', 'gl_init', 1 );

function gl_init()
{

	include( get_template_directory() . '/classes/contact_form.php');
	include( get_template_directory() . '/classes/basket_form.php');
	$contact_form = new Contact_Form();
	$basket_form = new Basket_Form();


	register_post_type('artists',
			array(
					'labels' => array(
							'name' => 'Художники',
							'singular_name' => 'Художники'
					),
					'public' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'artists'),
					'supports' => array('title', 'editor', 'excerpt', 'thumbnail')
			)
	);
	register_taxonomy_for_object_type('category', 'artists');

	register_post_type('baguette
',
			array(
					'labels' => array(
							'name' => 'Багет',
							'singular_name' => 'Багет'
					),
					'public' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'baguette'),
					'supports' => array('title', 'thumbnail')
			)
	);
	register_taxonomy_for_object_type('category', 'baguette');

	register_taxonomy(
			'keywoard',
			array(
					'post',
			),
			array(
					'label' => 'Ключевые слова',
					'hierarchical' => false,
			)
	);

	register_taxonomy(
			'genre',
			array(
					'post',
			),
			array(
					'label' => 'Жанр',
					'hierarchical' => false,
			)
	);

	register_taxonomy(
			'style',
			array(
					'post',
			),
			array(
					'label' => 'Стиль',
					'hierarchical' => false,
			)
	);

	register_taxonomy(
			'interior',
			array(
					'post',
			),
			array(
					'label' => 'Стиль интерьера',
					'hierarchical' => false,
			)
	);

	register_taxonomy(
			'technique',
			array(
					'post',
			),
			array(
					'label' => 'Техника',
					'hierarchical' => false,
			)
	);

	add_theme_support('post-thumbnails');

	add_theme_support('category-thumbnails');

	add_shortcode( 'contact_form', array($contact_form, 'render') );

	add_shortcode( 'basket_form', array($basket_form, 'render') );

	add_action( 'admin_menu', 'gl_admin_menu' );

	add_action( 'admin_enqueue_scripts', 'load_custom_admin_links' );

}

function gl_admin_menu() {
	admin_menu_messages();
	admin_menu_orders();
}

function admin_menu_messages(){
	global $wpdb;
	$allCount = $wpdb->get_results( 'SELECT COUNT(*) as count FROM `gl_contact_form` WHERE is_new=1' );
	$allCount = $allCount[0]->count;
	$allCount = ($allCount > 9) ? '9+' : $allCount;
	$buble = ($allCount > 0) ? '<span class="awaiting-mod"><span class="pending-count">'.$allCount.'</span></span>' : '';
	add_menu_page( 'Сообщения', 'Сообщения '.$buble, 'manage_options', 'messages', 'gl_contact_form_messages', 'dashicons-email', 5 );
}
function admin_menu_orders(){
	global $wpdb;
	$allCount = $wpdb->get_results( 'SELECT COUNT(*) as count FROM `gl_basket` WHERE is_new=1' );
	$allCount = $allCount[0]->count;
	$allCount = ($allCount > 9) ? '9+' : $allCount;
	$buble = ($allCount > 0) ? '<span class="awaiting-mod"><span class="pending-count">'.$allCount.'</span></span>' : '';
	add_menu_page( 'Заказы', 'Заказы '.$buble, 'manage_options', 'orders', 'gl_orders', 'dashicons-cart', 6 );
}

function gl_contact_form_messages(){
	get_template_part('admin/contact_form_messages');
}

function gl_orders(){
	get_template_part('admin/orders');
}

function load_custom_admin_links() {
	wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin/css/index.css', false, '1.0.0' );
	wp_register_script( 'custom_wp_admin_js', get_template_directory_uri() . '/admin/js/index.js', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
	wp_enqueue_script( 'custom_wp_admin_js' );
}