<?php

$args = array( 'numberposts' => -1, 'post_type' => 'artists' );
$artists  = get_posts( $args );


$args = array(
    'type'                     => 'post',
    'child_of'                 => 0,
    'parent'                   => '',
    'orderby'                  => 'name',
    'order'                    => 'ASC',
    'hide_empty'               => false,
    'hierarchical'             => 1,
    'exclude'                  => '',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false
);
$categories  = get_categories($args);

$args['taxonomy'] = 'keywoard';
$keywords = get_categories($args);


$args['taxonomy'] = 'genre';
$genres = get_categories($args);

$args['taxonomy'] = 'style';
$styles = get_categories($args);


$args['taxonomy'] = 'technique';
$techniques = get_categories($args);



?>

<div class="search_form">
    <form id="simple_search_form" action="/" method="get">
        <div class="custom_search">
            <div class="input-group col-lg-4 col-md-6 col-sm-12 right_side">
                <input class="search_field form-control" type="text" name="s" placeholder="Поиск картин" aria-label="Поиск картин">
                <span class="input-group-addon submit_simple_search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
            </div>
        </div>
    </form>
    <div style="clear: both"></div>
    <a class="open_form" href="#advanced_search"  data-toggle="collapse">Расширенный поиск</a>

    <div id="advanced_search" class="advanced_search hidden_div">
        <form id="advanced_search_form" action="/" method="get">
            <label>Выберите художника</label>
            <select data-placeholder="Художник" name="artists[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($artists as $artist){
                    echo "<option value='$artist->ID'>$artist->post_title</option>";
                }
                ?>
            </select>

            <label>Выберите категорию</label>
            <select data-placeholder="Категории" name="category[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($categories as $category){
                    echo "<option value='$category->term_id'>$category->name</option>";
                }
                ?>
            </select>

            <label>Выберите ключевые слова</label>
            <select data-placeholder="Ключевые слова" name="keyword[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($keywords as $keyword){
                    echo "<option value='$keyword->term_id'>$keyword->name</option>";
                }
                ?>
            </select>

            <label>Выберите жанры</label>
            <select data-placeholder="Жанры" name="genre[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($genres as $genre){
                    echo "<option value='$genre->term_id'>$genre->name</option>";
                }
                ?>
            </select>

            <label>Выберите стиль</label>
            <select data-placeholder="Стиль, направление" name="style[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($styles as $style){
                    echo "<option value='$style->term_id'>$style->name</option>";
                }
                ?>
            </select>

            <label>Выберите технику</label>
            <select data-placeholder="Техника" name="technique[]" multiple class="chosen-select" tabindex="8">
                <?php
                foreach($techniques as $technique){
                    echo "<option value='$technique->term_id'>$technique->name</option>";
                }
                ?>
            </select>

            <label>Год: &nbsp;от&nbsp;&nbsp;</label> <input class="form-control select_year" name="from_year" type="number">&nbsp;&nbsp;г.&nbsp;&nbsp; <label>до</label>&nbsp;&nbsp; <input class="form-control select_year" name="to_year" type="number">&nbsp;&nbsp; г.
            <div style="padding: 10px 0px"></div>
            <label id="price_range_f_form">Цена&nbsp;&nbsp;  От&nbsp;&nbsp;<span class="min"></span>&nbsp;&nbsp;.руб -  до&nbsp;&nbsp;<span class="max"></span>&nbsp;&nbsp;.руб&nbsp;&nbsp;</label>
            <div data-min="1800" data-max="350000" id="range-input" class="input_range" style="margin:31px 0px">
            </div>
            <div style="text-align: center">
                <button type="button" class="btn btn-primary submit_advanced_search">Поиск</button>
            </div>
        </form>

    </div>
    <div style="clear: both"></div>
</div>