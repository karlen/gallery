<?php
    $flashes = $GLOBALS['wp_query']->query_vars['flashes'];
?>
<div class="contact_us_wrap">
    <h3>Форма обратной связи</h3>
    <div class="border"></div>
    <form method="post">
        <div class="container">
            <div class="form-row">
                <input class="form-control contact-form-input" id="form-name" type="text" name="contact_form_name"
                       placeholder="Ваше имя...">
                <input class="form-control contact-form-input"  id="form-email" type="text" name="contact_form_email"
                       placeholder="Email-адрес...">
                    <textarea class="form-control contact-form-input" id="form-text" name="contact_form_text"
                              placeholder="Текст сообщения..."></textarea>
                <div class ="flashes">
                    <?php
                    if(!empty($flashes)){
                        foreach($flashes['contact_form']['error'] as $error){
                            echo "<div class='error'>$error</div>";
                        }
                    }
                    ?>
                </div>
                <input type="submit" name="submit_contact_form" value="Отправить">
            </div>
        </div>
    </form>
</div>
