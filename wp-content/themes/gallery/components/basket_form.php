<?php
$flashes = $GLOBALS['wp_query']->query_vars['flashes'];
?>
<div class="contact_us_wrap basket_order_wrap">
    <h3>Оформить заказ</h3>
    <div class="border"></div>
    <form method="post">
        <div class="container">
            <div class="form-row">
                <input class="form-control contact-form-input" id="form-name" type="text" name="basket_form_name"
                       placeholder="Ваше имя...">
                <input class="form-control contact-form-input"  id="form-email" type="text" name="basket_form_email"
                       placeholder="Email-адрес...">
                <input class="form-control contact-form-input"  id="form-address" type="text" name="basket_form_address"
                       placeholder="Ваш адрес...">
                <input class="form-control contact-form-input"  id="form-number" type="text" name="basket_form_number"
                       placeholder="Ваш номер телефона...">
                <label></label>
                <select class="form-control contact-form-input" name="basket_form_address_type">
                    <option value="cамовывоз">Самовывоз</option>
                    <option value="доставка по Санкт-Петербургу">доставка по Санкт-Петербургу</option>
                    <option value="доставка по России">доставка по России</option>
                </select>
                <textarea class="form-control contact-form-input" id="form-text" name="basket_form_text"
                              placeholder="Текст сообщения..."></textarea>
                <div class ="flashes">
                    <?php
                    if(!empty($flashes)){
                        foreach($flashes['basket_form']['error'] as $error){
                            echo "<div class='error'>$error</div>";
                        }
                    }
                    ?>
                </div>
                <input type="submit" name="submit_basket_form" value="Отправить">
            </div>
        </div>
    </form>
</div>
