<?php

class Basket_Form
{
    protected $flashes = array(), $name, $email, $text, $address, $address_type, $number, $posts;

    public function __construct()
    {
        if(isset($_POST['submit_basket_form'])){
            $this->submit();
        }
    }


    public function submit()
    {

        if ($this->hasFlashes()) {
            return 0;
        }
        global $wpdb;

        if(empty($this->posts)){
            return 0;
        }

        $result = $wpdb->query($wpdb->prepare("INSERT INTO " . $wpdb->prefix . "basket
            (name, email, number, address, text, address_type, posts, is_new) VALUES
            (%s, %s, %s,%s, %s, %s, %s, %d)
            ",
            esc_html($this->name),
            esc_html($this->email),
            esc_html($this->number),
            esc_html($this->address),
            esc_html($this->text),
            esc_html($this->address_type),
            esc_html($this->posts),
            1
        ));

        setcookie ("cart", "", time() - 3600);

    }

    protected function hasFlashes()
    {

        $this->name = sanitize_text_field($_POST['basket_form_name']);
        $this->email = sanitize_email($_POST['basket_form_email']);
        $this->text = sanitize_text_field($_POST['basket_form_text']);
        $this->number = sanitize_text_field($_POST['basket_form_number']);
        $this->address = sanitize_text_field($_POST['basket_form_address']);
        $this->address_type = sanitize_text_field($_POST['basket_form_address_type']);
        $this->posts = $_COOKIE['cart'];

        if (strlen($this->name) <= 2 ) {
            $this->flashes['basket_form']['error'][] = 'Пожалуйста введите Ваше имя.';
        }


        if (strlen($this->address) <= 6 ) {
            $this->flashes['basket_form']['error'][] = 'Пожалуйста введите Ваш адрес.';
        }


        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->flashes['basket_form']['error'][] = 'Пожалуйста введите Ваш email-адрес.';
        }

        if (strlen($this->text) < 6) {
            $this->flashes['basket_form']['error'][] = 'Пожалуйста введите текст сообщения.';
        }


        if (isset($this->flashes['basket_form'])) {
            return true;
        } else {
            return false;
        }
    }

    protected function return_template_part($template_name, $part_name = null)
    {
        ob_start();
        get_template_part($template_name, $part_name);
        $var = ob_get_contents();
        ob_end_clean();
        return $var;
    }


    public function render()
    {
        set_query_var('flashes', $this->flashes);
        return $this->return_template_part('components/basket_form');
    }



}