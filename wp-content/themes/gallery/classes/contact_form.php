<?php

class Contact_Form
{
    protected $flashes = array(), $name, $email, $text;

    public function __construct()
    {
        if(isset($_POST['submit_contact_form'])){
            $this->submit();
        }
    }


    public function submit()
    {

        if ($this->hasFlashes()) {
            return 0;
        }
        global $wpdb;


        $result = $wpdb->query($wpdb->prepare("INSERT INTO " . $wpdb->prefix . "contact_form
            (name, email, text, is_new) VALUES
            (%s, %s, %s, %d)
            ",
            esc_html($this->name),
            esc_html($this->email),
            esc_html($this->text),
            1
        ));

    }

    protected function hasFlashes()
    {

        $this->name = sanitize_text_field($_POST['contact_form_name']);
        $this->email = sanitize_email($_POST['contact_form_email']);
        $this->text = sanitize_text_field($_POST['contact_form_text']);


        if (strlen($this->name) <= 2 ) {
            $this->flashes['contact_form']['error'][] = 'Пожалуйста введите Ваше имя.';
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->flashes['contact_form']['error'][] = 'Пожалуйста введите Ваш email-адрес.';
        }

        if (strlen($this->text) < 6) {
            $this->flashes['contact_form']['error'][] = 'Пожалуйста введите текст сообщения.';
        }


        if (isset($this->flashes['contact_form'])) {
            return true;
        } else {
            return false;
        }
    }

    protected function return_template_part($template_name, $part_name = null)
    {
        ob_start();
        get_template_part($template_name, $part_name);
        $var = ob_get_contents();
        ob_end_clean();
        return $var;
    }


    public function render()
    {
        set_query_var('flashes', $this->flashes);
        return $this->return_template_part('components/contact_form');
    }



}