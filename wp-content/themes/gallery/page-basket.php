<?php
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Gallery
 */
get_header();
global $post;
$post_data = get_fields($post);
$address = $post_data['add_address'];
$posts = $post_data['add_posts'];
$cart = $_COOKIE['cart'];
$cart = empty($cart) ? array() : explode(',',$cart);
$orders = array();
foreach($cart as $order){
    if(!empty($order)){
        $orders[] = get_post((int)$order);
    }
}
?>
    <div class="category_main_header">
        <h1>- <?php echo $post->post_title; ?> -</h1>
        <?php
        echo $post->post_content;
        ?>
        <div style="clear: both"></div>
        <div class="category_list_items">
            <?php
            if(!empty($orders)){
                foreach($orders as $single_order){
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($single_order->ID), 'thumbnail' );
                    $img = $thumb[0];
                    $name = $single_order->post_title;
                    $url = get_permalink($single_order);
                    $post_data = get_fields($single_order->ID);
                    $exist = ucfirst($post_data['status_exist']);
                    ?>
                    <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $single_order->ID; ?>" >
                        <div class="content">
                            <a href='<?php echo $url; ?>'>
                                <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                                <div class="title">-<?php echo $name; ?>-</div>
                            </a>
                            <div data-id="<?php echo $single_order->ID; ?>" class="title remove_from_basket">Удалить</div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div style="clear: both"></div>
        <?php
        if(!empty($orders)) {
            echo do_shortcode("[basket_form]");
        }else{
            echo "Ваша корзина пуста..";
        }
        ?>
        <div class="category_list_items">
            <?php
            foreach($posts as $post){
                $post = get_post($post);
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $img = $thumb[0];
                $name = $post->post_title;
                $url = get_permalink($post);
                $post_data = get_fields($post->ID);
                $exist = ucfirst($post_data['status_exist']);
                ?>
                <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $post->ID; ?>" >
                    <div class="content">
                        <a href='<?php echo $url; ?>'>
                            <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                            <div class="title">-<?php echo $name; ?>-</div>
                        </a>
                    </div>
                </div>

                <?php
            }
            ?>

        </div>

    </div>
<?php
get_footer();
?>