<?php
get_header();
global $wp_query;
$category = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$img = json_decode($category->term_thumbnail);
$args = array(
    'post_type' => 'any',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => get_query_var( 'taxonomy' ),
            'field' => 'id',
            'terms' => $category->term_id
        )
    )
);

$query = new WP_Query( $args );
$posts = $query->posts;
?>
    <div class="category_main_header">
        <h1>- <?php echo $category->name; ?> -</h1>
    <span>
        <?php
        if(!empty($img->url)){
            ?>
            <img width="200" src="<?php echo $img->url; ?>" />
            <?php
        }
        ?>
        <?php
        echo $category->description;
        ?>
    </span>
        <div style="clear: both"></div>
        <div class="category_list_items">

            <?php

            foreach($posts as $post){
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $img = $thumb[0];
                $name = $post->post_title;
                $url = get_permalink($post);
                $post_data = get_fields($post->ID);
                $exist = ucfirst($post_data['status_exist']);
                ?>


                <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $post->ID; ?>" >
                    <div class="content">
                        <a href='<?php echo $url; ?>'>
                            <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                            <div class="title">-<?php echo $name; ?>-</div>
                        </a>
                        <div class="info">
                            <p>Цена(руб.) <?php echo number_format($post_data['price'],0); ?></p>
                            <p>Размер <?php echo $post_data['size'];?></p>
                            <p>Наличие <?php echo $exist;?></p>
                        </div>
                        <div class="actions">
                            <span>
                                <a href='<?php echo $url; ?>'> Подробнее </a>
                            </span>

                            <span class="add_to_basket" data-id="<?php echo $post->ID; ?>">Купить</span>
                        </div>
                    </div>
                </div>

                <?php
            }

            ?>
        </div>

    </div>
<?php get_footer(); ?>