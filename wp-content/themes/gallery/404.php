<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Gallery
 */

get_header(); ?>
Page not found
<?php get_sidebar(); ?>
<?php get_footer(); ?>