jQuery(document).ready(function() {
    $('.chosen-select').chosen({
        width: '100%'
    });
    $('.advanced_search').addClass('collapse').removeClass('hidden_div');
    equal_divs();


    var slider = document.getElementById('range-input');
    var min =  parseInt( ($('#range-input').attr('data-min')).replace(/\./g, "") );
    var max =  parseInt( ($('#range-input').attr('data-max')).replace(/\./g, "") );

    noUiSlider.create(slider, {
        start: [min + 50000, max -50000],
        connect: true,
        step: 1000,
        range: {
            'min': min,
            'max': max
        }
    }).on('update', function(e){
        $('#price_range_f_form .min').html(toprice(parseInt(e[0])));
        $('#price_range_f_form .max').html(toprice(parseInt(e[1])));
    });


    $('#simple_search_form .submit_simple_search').on('click', function(){
        $('#simple_search_form').submit();
    });


    $('#advanced_search_form .submit_advanced_search').on('click', function(){

        var form = $('#advanced_search_form').serializeArray();
        var min_price = ($('#price_range_f_form .min').html()).replace(/\./g, "");
        var max_price = ($('#price_range_f_form .max').html()).replace(/\./g, "");
        form.push({
            name: 'min',
            value:  parseInt(min_price)
        });

        form.push({
            name: 'max',
            value:  parseInt(max_price)
        });

        var sendUrl  = '/?s=&';

        $.each(form, function(k, v) {
            var ky = v.name;
            var vl = v.value;
            sendUrl +=  ''+ky+'='+vl+'&';
        });
        window.location.href = sendUrl;
    });


    $('.add_to_basket').on('click', function(){
        console.log('sadasd');
        basketplus($(this).attr('data-id'));
    });

    $('.remove_from_basket').on('click', function(){
        basketminus($(this).attr('data-id'));
    });

    (new backgroundSlider()).init();

    var owl = $('.owl-carousel');

    $('.owl-carousel.baguettes_slider').owlCarousel({
        margin:10,
        loop:true,
        autoWidth:true
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY>0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });

    $('.baguettes_slider .baguette_item').on('click', function(){
        var frame_img = $(this).attr('data-img');
        $('.main_container .wrapper .single_item .left_side img').addClass('baguette_frame').css('border-image-source','url('+frame_img+')');
    });

});

jQuery(window).resize(function() {
    equal_divs();
});

jQuery(window).scroll(function(e){

});

function basketplus(id){
    if(setCookie('cart', id)){
        $('.basket_count').html(parseInt($('.basket_count').html()) + 1);
    }
}

function basketminus(id){
    if(parseInt($('.basket_count').html()) >= 1) {
        setCookie('cart', id, {action: 'delete'});
        $('.item[data-id="'+id+'"]').hide(700);
        $('.basket_count').html(parseInt($('.basket_count').html()) - 1);
        if(parseInt($('.basket_count').html()) == 0){
            $('.basket_order_wrap').fadeOut(1500, function(){
                $('.basket_order_wrap').remove();
            });
        }
    }
}

function equal_divs(){

    if($(window).width() > 977){
        var divone = jQuery(".main_content > .sidebar").height();
        var divtwo = jQuery(".main_content > .wrapper").height();
        var maxdiv = Math.max(divone, divtwo);

        jQuery(".main_content > .sidebar").css('min-height', maxdiv);
        jQuery(".main_content > .wrapper").css('min-height', maxdiv);
    }else {
        jQuery(".main_content > .sidebar").css('min-height', 0);
        jQuery(".main_content > .wrapper").css('min-height', 0);
    }

}

function toprice(x){
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(".");
}

function getCookie(name) {
    function escape(s) { return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1'); };
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : false;
}

function setCookie(name, value, options) {
    options = options || {};
    var action = options.hasOwnProperty("action") ? options.action : "add";
    var expires = options.hasOwnProperty("expires") ? options.expires : 3600;
    var cookieValue = (getCookie("cart") !== false) ? getCookie("cart") : '';
    var added = false;


    options.expires = '';
    options.PATH = '/';

    value = encodeURIComponent(value);

    if(action == "add") {
        cookieValue = (cookieValue.length > 0) ? cookieValue.split(',') : [];

        if(cookieValue.indexOf(value) == -1) {
            cookieValue.push(value);
            added = true
        }

    } else if(action == "delete") {
        cookieValue = cookieValue.split(',') || [];
        if(cookieValue.length > 0) {
            if(cookieValue.indexOf(value) != -1) {
                cookieValue[cookieValue.indexOf(value)] = null;
            }
        }
    }
    cookieValue = cookieValue.filter(function(v){return v!==''});
    cookieValue = cookieValue.join(',');

    var updatedCookie = name + "=" + cookieValue;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
    return added;
}

function backgroundSlider() {

    this.images = window.sliderImages;

    this.active = 0;

    this.init = function(){
        if(this.images.length == 0){
            this.images = [
                '/wp-content/themes/gallery/assets/images/slider/1.jpg',
                '/wp-content/themes/gallery/assets/images/slider/2.jpg',
                '/wp-content/themes/gallery/assets/images/slider/3.jpg'
            ];
        }
        $('body').css('background-image', 'url('+this.images[0]+')');
        this.attachEvents();
    };

    this.attachEvents = function(){
        var _self = this;
        $('.slider.prev').on('click', function(){
            _self.prev();
        });
        $('.slider.next').on('click', function(){
            _self.next();
        });
        _self.loadImages();
    };

    this.loadImages = function(){
        for(var i=1; i< this.images.length; i++) {
            var img = new Image();
            img.src = this.images[i];
            img.style.display = 'none';
            document.head.appendChild(img);
        }
    };

    this.slideLeft = function() {
        var _self = this;
        $('body').css('background-image', $('body').css('background-image')+', url('+this.images[this.active]+')');
        $('body').addClass('to_left_background');
        setTimeout(function(){
            $('body').css('background-image', 'url('+_self.images[_self.active]+')').removeClass('to_left_background');
        },850);
    };

    this.slideRight = function(){
        var _self = this;
        $('body').css('background-image', $('body').css('background-image')+', url('+this.images[this.active]+')');
        $('body').addClass('to_right_background');
        setTimeout(function(){
            $('body').css('background-image', 'url('+_self.images[_self.active]+')').removeClass('to_right_background');
        },850);
    };

    this.prev = function(){
        this.active = (this.active == 0) ? this.images.length-1 : this.active -1 ;
        this.slideRight();
    };

    this.next = function(){
        this.active = (this.active == this.images.length -1) ? 0 : this.active + 1 ;
        this.slideLeft();
    };
}

(function() {
    if (window.pluso)if (typeof window.pluso.start == "function") return;
    if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
    }
})();