<?php
/**
 * The template for displaying Search result pages.
 *
 * @package WordPress
 * @subpackage Gallery
 */

get_header();
if(!empty($_GET['s'])){

    global $query_string;

    $query_args = explode("&", $query_string);
    $search_query = array();

    if( strlen($query_string) > 0 ) {
        foreach($query_args as $key => $string) {
            $query_split = explode("=", $string);
            $search_query[$query_split[0]] = urldecode($query_split[1]);
        } // foreach
    } //if

    $search_posts = new WP_Query($search_query);
    $posts = $search_posts->posts;

}else{


    $search_categories =  ($_GET['category']);
    $search_artists =  ($_GET['artists']);
    $search_keywords =  ($_GET['keyword']);
    $search_genres =  ($_GET['genre']);
    $search_styles =  ($_GET['style']);
    $search_techniques =  ($_GET['technique']);
    $from_year =  esc_attr((int)$_GET['from_year']);
    $to_year =  esc_attr((int)$_GET['to_year']);
    $min =  esc_attr((int)$_GET['min']);
    $max =  esc_attr((int)$_GET['max']);


    $search_categories = empty($search_categories) ? array() : $search_categories;
    foreach($search_categories as $cat_key => $cat_val){
        $search_categories[$cat_key] = (int)$cat_val;
    }

    $search_keywords = empty($search_keywords) ? array() : $search_keywords;
    foreach($search_keywords as $keyword_key => $keyword_val){
        $search_keywords[$keyword_key] = (int)$keyword_val;
    }

    $search_genres = empty($search_genres) ? array() : $search_genres;
    foreach($search_genres as $genre_key => $genre_val){
        $search_genres[$genre_key] = (int)$genre_key;
    }

    $search_styles = empty($search_styles) ? array() : $search_styles;
    foreach($search_styles as $style_key => $style_val){
        $search_styles[$style_key] = (int)$style_val;
    }

    $search_techniques = empty($search_techniques) ? array() : $search_techniques;
    foreach($search_techniques as $technique_key => $technique_val){
        $search_techniques[$technique_key] = (int)$technique_val;
    }

    $search_artists = empty($search_artists) ? array() : $search_artists;
    foreach($search_artists as $artist_key => $artist_val){
        $search_artists[$artist_key] = (int)$artist_val;
    }


    /**
     * Create tax query
     */

    $tax_query = array();

    if(!empty($search_categories)){
        $tax_query[] =  array(
            'taxonomy' => 'category',
            'terms' => $search_categories,
            'field' => 'term_id',
            "operator" => "IN"
        );
    }

    if(!empty($search_keywords)){
        $tax_query[] =   array(
            'taxonomy' => 'keywoard',
            'terms' => $search_keywords,
            'field' => 'term_id',
            "operator" => "IN"
        );
    }

    if(!empty($search_genres)){
        $tax_query[] =   array(
            'taxonomy' => 'genre',
            'terms' => $search_genres,
            'field' => 'term_id',
            "operator" => "IN"
        );
    }

    if(!empty($search_styles)){
        $tax_query[] =   array(
            'taxonomy' => 'style',
            'terms' => $search_styles,
            'field' => 'term_id',
            "operator" => "IN"
        );
    }

    if(!empty($search_techniques)){
        $tax_query[] =   array(
            'taxonomy' => 'technique',
            'terms' => $search_techniques,
            'field' => 'term_id',
            "operator" => "IN"
        );
    }

    if(!empty($tax_query)){
        $tax_query["relation"] = 'OR';
    }


    /**
     * Create meta query
     */
    $meta_query = array();
    if(!empty($search_artists)){
        $meta_query[] =  array(
            'key'       => 'xudozniki',
            'value'     => $search_artists,
            'compare'   => 'IN',
        );
    }

    if(!empty($min) && !empty($max)){
        $min = (int)$min;
        $max = (int)$max;
        $meta_query[] =  array(
            'key'       => 'price',
            'value'     => array($min, $max),
            'compare'   => 'BETWEEN',
            'type' => 'NUMERIC'
        );
    }


    if(!empty($from_year) || !empty($to_year)){
        $from_year = empty($from_year) ? 0 :$from_year;
        $to_year = empty($to_year) ? date("Y") +1 : $to_year;
        $meta_query[] =  array(
            'key'       => 'year',
            'value'     => array($from_year, $to_year),
            'compare'   => 'BETWEEN',
            'type' => 'NUMERIC'
        );
    }

    if(!empty($meta_query)){
        $tax_query["relation"] = 'OR';
    }
    $search_posts = new WP_Query(array(
        'post_type' => 'post',
        'showposts' => -1,
        'tax_query' => $tax_query,
        'meta_query'    => $meta_query,
        'orderby' => 'title',
        'order' => 'DESC'
    ));



    $posts = $search_posts->posts;
}

?>
    <div class="category_list_items">

        <?php

        foreach($posts as $post){
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
            $img = $thumb[0];
            $name = $post->post_title;
            $url = get_permalink($post);
            $post_data = get_fields($post->ID);
            $exist = ucfirst($post_data['status_exist']);
            ?>


            <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $post->ID; ?>" >
                <div class="content">
                    <a href='<?php echo $url; ?>'>
                        <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                        <div class="title">-<?php echo $name; ?>-</div>
                    </a>
                    <?php
                    if($post->post_type == 'post'){
                        ?>

                        <div class="info">
                            <p>Цена(руб.) <?php echo number_format($post_data['price'],0); ?></p>
                            <p>Размер <?php echo $post_data['size'];?></p>
                            <p>Наличие <?php echo $exist;?></p>
                        </div>

                        <?php
                    }
                    ?>
                    <div class="actions">

                        <span>
                                <a href='<?php echo $url; ?>'> Подробнее </a>
                            </span>

                        <?php
                        if($post->post_type == 'post'){
                            ?>
                            <span class="add_to_basket" data-id="<?php echo $post->ID; ?>">Купить</span>

                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
        }

        ?>

    </div>
<?php get_footer(); ?>