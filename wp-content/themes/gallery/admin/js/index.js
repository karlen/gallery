
function messagesTable(){

    this.url = '';

    this.init = function(){
        if(jQuery('.messages_table').length <= 0){
            return;
        }
        this.url =  jQuery('.messages_table').attr('data-url') || false;
        if(this.url){
            this.attachEvents();
        }
    }

    this.attachEvents = function(){
        var _self = this;
        jQuery('.messages_table .check_all_remove_item').on('click', function(){
            if(jQuery(this).is(':checked')){
                jQuery('.remove_item').each(function(){
                    jQuery(this).prop('checked', 'checked');
                });
            }else {
                jQuery('.remove_item').each(function(){
                    jQuery(this).removeProp('checked');
                });
            }
        });

        jQuery('.remove_item').click(function(){
            if(jQuery('.remove_item:checked').length !== jQuery('.remove_item').length){
                jQuery('.check_all_remove_item').removeProp('checked');
            }else if(jQuery('.remove_item:checked').length == jQuery('.remove_item').length){
                jQuery('.check_all_remove_item').prop('checked', 'checked');
            }
        });

        jQuery('.messages_table .messages_remove_checked_items_button').on('click', function(){
            _self.removeChecked();
        });
    }

    this.removeChecked = function(){
        var checkedItems = [];
        var query = '';
        jQuery('.messages_table .remove_item:checked').each(function(){
            checkedItems.push(jQuery(this).val());
            query += '&remove_item[]='+jQuery(this).val();
        });

        if(checkedItems.length > 0){
            window.location.href = this.url+query;
        }
    }

}

jQuery(document).ready(function(){
    (new messagesTable()).init();
});
