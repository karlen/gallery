<?php

class orders {
    protected $page, $limit = 20, $count, $orders, $totalPages, $prevPage, $nextPage,$url, $post_id = 0;

    public function __construct(){
        global $wpdb;

        $this->removeItems();

        $this->url = '/wp-admin/admin.php?page=orders';

        $this->page = (isset($_GET['paged']) && $_GET['paged'] != '0') ? (int) $_GET['paged'] : 1;

        $allCount = $wpdb->get_results( 'SELECT COUNT(*) as count FROM `gl_basket` ' );
        $this->count = $allCount[0]->count;

        $this->totalPages = ceil($this->count / $this->limit);
        $this->page = ($this->page > $this->totalPages) ? $this->totalPages : $this->page;
        $this->page = ($this->page < 1) ? 1 : $this->page;

        $this->prevPage = ($this->page > 1) ? $this->page - 1 : 1;
        $this->nextPage = ($this->page < $this->totalPages) ? $this->page + 1 : $this->totalPages;

        $this->orders = $this->getOrders();

    }

    protected function removeItems() {
        global $wpdb;
        if( isset($_GET['remove_item']) && !empty($_GET['remove_item']) ){
            $remove_id = $_GET['remove_item'];
            foreach($remove_id as $id){
                $id = (int)$id;
                $wpdb->delete( 'gl_basket', array( 'id' => $id ));
            }
        }
    }

    protected function getOrders(){
        global $wpdb;
        $limit = (($this->page - 1) * $this->limit).','.$this->limit ;
        $orders =  $wpdb->get_results( "SELECT * FROM  `gl_basket`  ORDER BY id DESC LIMIT $limit");
        $ordersID = array();
        foreach($orders as $order){
            $ordersID[] = (int)$order->id;
        }

        $inQuery = '(' . implode(',', array_map('intval', $ordersID)) . ')';
        $wpdb->query( 'UPDATE gl_basket SET is_new = 0 WHERE id IN '.$inQuery.'  AND is_new=1');
        return $orders;
    }

    protected function getPagination() {

        $navPrev = ($this->page == 1) ?
            '<span class="tablenav-pages-navspan" aria-hidden="true">«</span><span class="tablenav-pages-navspan" aria-hidden="true">‹</span>' :

            '<a class="first-page" href="'.$this->url.'"><span aria-hidden="true">«</span></a>
             <a class="prev-page"  href="'.$this->url.'&paged='.$this->prevPage.'"><span aria-hidden="true">‹</span></a>';

        $navNext = ($this->page == $this->totalPages) ?
            '<span class="tablenav-pages-navspan" aria-hidden="true">›</span><span class="tablenav-pages-navspan" aria-hidden="true">»</span>' :

            '<a class="next-page" href="'.$this->url.'&paged='.$this->nextPage.'"><span aria-hidden="true">›</span></a>
             <a class="last-page" href="'.$this->url.'&paged='.$this->totalPages.'"><span aria-hidden="true">»</span></a></span>';


        $numberedForm =  '<form action="/wp-admin/admin.php" method="get" style="display:inline-block">
                            <input type="hidden" name="page" value="orders" />
                            <input class="current-page" id="current-page-selector" type="text" min="1" name="paged" value="'.$this->page.'" size="3" aria-describedby="table-paging">
                         </form>';
        $html = '
            <div class="tablenav bottom">
                <div class="tablenav-pages"><span class="displaying-num">'.$this->count.' items</span>

                    '.$navPrev.'

                    <span id="table-paging" class="paging-input">

                    '.$numberedForm.'

                     of <span class="total-pages">'.$this->totalPages.'</span></span>

                    '.$navNext.'

                </div>
            </div>';

        return $html;
    }

    public function render(){
        $messages = '';
        foreach($this->orders as $value){

            $style = $value->is_new == 0 ? "" : "color:green;";
            $order_posts = explode(',',$value->posts);
            $order_posts = !empty($order_posts) ? $order_posts : array();
            $postsList = '';

            foreach ($order_posts as $post_order) {
                if(!empty($post_order)){
                    $post_order = get_post($post_order);
                    $postsList .= "<a target='blank' href='".get_permalink($post_order)."'>$post_order->post_title</a><hr>";
                }
            }

            $messages .= "
                                <tr>
                                    <td style='max-width:50px'><input type='checkbox' style='margin: 0px !important;' class='remove_item' name='remove_item' value='$value->id'></td>
                                    <td style='max-width:100px; $style'>$value->name</td>
                                    <td style='max-width:190px'>$value->email</td>
                                    <td>$value->text</td>
                                    <td style='max-width:190px'>$value->number</td>
                                    <td style='max-width:190px'>$value->address</td>
                                    <td style='max-width:190px'>$value->address_type</td>
                                    <td style='max-width:190px'>$postsList</td>
                                </tr>";
        }

        $pagination = $this->getPagination();


        $html = '
        <div class="wrap messages_table" data-url="'.$this->url.'" >
                    <h2>Заказы</h2>
                    <div class="alignleft actions bulkactions" style="margin-bottom:10px;width: 100%">
                        '.$pagination.'
                        <div class="show_results">
                            <table class="wp-list-table widefat  striped">
                                <thead>
                                <tr>
                                    <td><input type="checkbox" style="margin: 0px !important;" class="check_all_remove_item" /></td>
                                    <td style="max-width:100px">Имя</td>
                                    <td style="max-width:100px">Email-адрес</td>
                                    <td style="max-width:110px">Телефон</td>
                                    <td style="max-width:110px">Сообщение</td>
                                    <td style="max-width:100px">Адрес</td>
                                    <td style="max-width:100px">Тип</td>
                                    <td style="max-width:100px">Заказы</td>
                                </tr>
                                </thead>
                                <tbody>
                                    '.$messages.'
                                </tbody>
                            </table>
                        </div>

                        '.$pagination.'
                        <input  type="button" class="button button-primary messages_remove_checked_items_button" value="Удалить заказы" style="margin-top:10px;" />

                     </div>
        </div><div style="clear:both"></div>';

        echo $html;
    }
}

$orders = new orders();
$orders->render();
