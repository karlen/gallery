<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.WordPress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gallery
 */
get_header();
//$args = array(
//    'show_option_all'    => '',
//    'orderby'            => 'name',
//    'order'              => 'ASC',
//    'hide_empty'         => 0,
//    'taxonomy'           => 'category'
//);
//$categories = get_categories( $args );

$args = array( 'numberposts' => -1, 'post_type' => 'artists' );
$categories  = get_posts( $args );
?>
    <div class="category_list_home">

        <?php
        foreach($categories as $category){
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($category->ID), 'thumbnail' );
            $img = !empty($thumb[0]) ? $thumb[0] : get_template_directory_uri().'/assets/images/noimage.jpg';
            $name = $category->post_title;
            $url = get_permalink($category->ID);
            echo "<a href='{$url}'><div title='{$name}' class='col-lg-3 col-md-4 col-sm-4 cat_item' style='background-image: url({$img})'><div class='cat_name'>{$name}</div></div></a>";
        }

        ?>

    </div>

<?php get_footer();

?>
