<?php
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Gallery
 */
get_header();
global $post;
$post_data = get_fields($post);
$address = $post_data['add_address'];
$posts = $post_data['add_posts'];
?>
    <div class="category_main_header">
        <h1>- <?php echo $post->post_title; ?> -</h1>
        <?php
        $content = apply_filters( 'the_content', $post->post_content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        echo $content;
        ?>
        <div style="clear: both"></div>
        <div class="category_list_items">

            <?php

            foreach($posts as $post){
                $post = get_post($post);
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $img = $thumb[0];
                $name = $post->post_title;
                $url = get_permalink($post);
                $post_data = get_fields($post->ID);
                $exist = ucfirst($post_data['status_exist']);
                ?>


                <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $post->ID; ?>" >
                    <div class="content">
                        <a href='<?php echo $url; ?>'>
                            <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                            <div class="title">-<?php echo $name; ?>-</div>
                        </a>
                    </div>
                </div>

                <?php
            }


            ?>

        </div>

        <?php
        if(!empty($address) && !empty($address['lat']) && !empty($address['lng'])){
            ?>
            <iframe style="margin: 15px auto" src="https://maps.google.com/maps?q=<?php echo $address['lat']; ?>,<?php echo $address['lng']; ?>&hl=es;z=14&amp;output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <?php
        }

        ?>

    </div>
<?php
get_footer();
?>