<?php
/**
 * The template for displaying single posts.
 *
 * @package WordPress
 * @subpackage Gallery
 */

get_header();
global $post;

$meta_query_args = array(
    'post_type' => 'any',
    'order'     => 'DESC',
    'meta_key' => 'xudozniki',
    'posts_per_page' => -1,
    'orderby'   => 'meta_value', //or 'meta_value_num'
    'meta_query' => array(
        array('key' => 'xudozniki',
            'value' => $post->ID
        )));
$query = new WP_Query( $meta_query_args );
$posts = $query->posts;
$image = get_the_post_thumbnail_url($post);

?>
    <div class="category_main_header">
        <h1>- <?php echo $post->post_title; ?> -</h1>
    <span>
        <?php
        if(!empty($image)){
            ?>
            <img width="200" src="<?php echo $image; ?>" />
            <?php
        }
        ?>
        <?php
        echo $post->post_content;
        ?>
    </span>
        <div style="clear: both"></div>
        <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="facebook,google,vkontakte,odnoklassniki,twitter"></div>
        <div class="category_list_items">

            <?php

            foreach($posts as $post){
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $img = $thumb[0];
                $name = $post->post_title;
                $url = get_permalink($post);
                $post_data = get_fields($post->ID);
                $exist = ucfirst($post_data['status_exist']);
                ?>


                <div title='<?php echo $name; ?>' class='col-lg-4 col-md-4 col-sm-4 item' data-id="<?php echo $post->ID; ?>" >
                    <div class="content">
                        <a href='<?php echo $url; ?>'>
                            <div class="image" style='background-image: url(<?php echo $img; ?>)'></div>
                            <div class="title">-<?php echo $name; ?>-</div>
                        </a>
                        <div class="info">
                            <p>Цена(руб.) <?php echo number_format($post_data['price'],0); ?></p>
                            <p>Размер <?php echo $post_data['size'];?></p>
                            <p>Наличие <?php echo $exist;?></p>
                        </div>
                        <div class="actions">
                            <span>
                                <a href='<?php echo $url; ?>'> Подробнее </a>
                            </span>

                            <span class="add_to_basket" data-id="<?php echo $post->ID; ?>">Купить</span>
                        </div>
                    </div>
                </div>

                <?php
            }

            ?>

        </div>

    </div>

<?php get_footer(); ?>