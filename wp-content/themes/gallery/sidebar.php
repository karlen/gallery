<?php


$args = array( 'numberposts' => -1, 'post_type' => 'artists' );
$artists  = get_posts( $args );
?>

<li class="artists">Художники
    <ul>
        <?php
        foreach($artists as $artist){
            $name = $artist->post_title;
            $url = get_permalink($artist);
            ?>
            <li class="cat-item">
                <a href="<?php echo $url; ?>"><?php echo $name; ?></a>
            </li>
            <?php
        }
        ?>
</ul>
</li>

<?php

/**
 * List of categories
 */
$args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'order'              => 'ASC',
    'style'              => 'list',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Категории',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'category',
    'walker'             => null
);
wp_list_categories( $args );


/**
 * List of keywords
 */
$args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'order'              => 'ASC',
    'style'              => 'list',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Ключевые слова',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'keywoard',
    'walker'             => null
);
wp_list_categories( $args );

/**
 * List of genres
 */
$args = array(
    'show_option_all'    => '',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'orderby'            => 'name',
    'order'              => 'ASC',
    'style'              => 'list',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Жанры',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'genre',
    'walker'             => null
);
wp_list_categories( $args );

/**
 * List of styles
 */
$args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'order'              => 'ASC',
    'style'              => 'list',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Стиль',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'style',
    'walker'             => null
);
wp_list_categories( $args );

/**
 * List of  interior styles
 */
$args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'order'              => 'ASC',
    'style'              => 'list',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Стиль интерьера',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'interior',
    'walker'             => null
);
wp_list_categories( $args );


/**
 * List of technique
 */
$args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'order'              => 'ASC',
    'style'              => 'list',
    'show_option_none'    => ' ',
    'hide_title_if_empty' => true,
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 1,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => 1,
    'title_li'           => 'Техника',
    'number'             => null,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'technique',
    'walker'             => null
);
wp_list_categories( $args );
