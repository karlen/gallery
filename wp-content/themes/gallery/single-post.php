<?php
/**
 * The template for displaying single posts.
 *
 * @package WordPress
 * @subpackage Gallery
 */

get_header();
global $post;
$post_data = get_fields($post);
$artist = $post_data['xudozniki'];
$image = get_the_post_thumbnail($post, 'full');
$exist = ucfirst($post_data['status_exist']);


$keywoards = get_the_term_list($post->ID, 'keywoard');
$genre = get_the_term_list($post->ID, 'genre');
$style = get_the_term_list($post->ID, 'style');
$interior = get_the_term_list($post->ID, 'interior');
$technique = get_the_term_list($post->ID, 'technique');
$categories = get_the_term_list($post->ID, 'category');
$year = $post_data['year'];

$baguettes = $post_data['baguette'];

?>
    <div class="single_item">
        <p class="title">- <?php the_title(); ?> -</p>
        <p class="artist"><?php echo "<a href='".get_permalink($artist->ID)."'> ".$artist->post_title."</a>"; ?></p>
        <div class="left_side col-lg-7 col-md-6 col-sm-12">
            <?php echo $image; ?>
        </div>
        <div class="right_side col-lg-5 col-md-6 col-sm-12">
            <div class="description">
                <?php echo $post_data['short_description']; ?>
                <?php if(!empty($year)){
                    ?>
                    <hr>Год: <?php echo $year; ?>
                    <?php
                }?>
                <?php if(!empty($categories)){
                    ?>
                    <hr>Категории: <?php echo $categories;?>
                    <?php
                }?>

                <?php if(!empty($keywoards)){
                    ?>
                    <hr>Ключевые слова: <?php echo $keywoards; ?>
                    <?php
                }?>

                <?php if(!empty($style)){
                    ?>
                    <hr>Стиль, направление: <?php echo $style;?>
                    <?php
                }?>

                <?php if(!empty($interior)){
                    ?>
                    <hr>Стиль интерьера: <?php echo $interior;?>
                    <?php
                }?>

                <?php if(!empty($genre)){
                    ?>
                    <hr>Жанр: <?php echo $genre;?>
                    <?php
                }?>


                <?php if(!empty($technique)){
                    ?>
                    <hr>Техника: <?php echo $technique;?>
                    <?php
                }?>
                <hr>Размер: <span class="specification"><?php echo $post_data['size']; ?></span>
                <hr>Статус: <span class="specification"><?php echo $exist; ?></span>
                <hr>Цена: <span class="specification"><?php echo number_format($post_data['price'],0); ?> .руб</span>
            </div>
            <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="facebook,google,vkontakte,odnoklassniki,twitter"></div>
            <div class="buy">
                <button type="button" data-id="<?php echo $post->ID; ?>" class="btn btn-primary add_to_basket">Купить</button>
            </div>
        </div>
        <?php
        if(!empty($baguettes)){
            ?>
            <div style="clear: both"></div>
            <span class="baguette_item_info">
            Эту картину можно оформить в раму, нажмите на фото багета:</span>
            <div class="baguettes_slider owl-carousel owl-theme">
                <?php
                foreach($baguettes as $baguette){
                    $img = get_the_post_thumbnail_url($baguette->ID);
                    $baguette_data = get_fields($baguette);
                    $id = $baguette->ID;
                    $price = $baguette_data['baguette_price'];
                    $data_img = $baguette_data['baguette_img'];
                    echo "<div data-id='$id' data-price='$price' data-img='$data_img' class='baguette_item' style='background-image: url($img)'></div>";
                }
                ?>

            </div>
            <div class="baguette_info">

            </div>
            <?php
        }
        ?>
        <div style="clear: both"></div>
        <div class="content_description">
            <?php echo $post->post_content; ?>
        </div>
    </div>


<?php get_footer(); ?>