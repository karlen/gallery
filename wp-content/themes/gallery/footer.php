</div>
<?php wp_footer(); ?>
</div>
<!--
Footer content
-->
<div class="footer row">
    <div class="container">
        <ul class="col-lg-8 col-md-8 col-sm-12 nav_footer">
            <?php
            wp_list_pages( array(
                'title_li'    => '',
                'child_of'    => 0,
            ) );
            ?>
        </ul>
        <div class="col-lg-4 col-md-4 col-sm-12 footer_logo">

        </div>
    </div>
</div>
<!--
Footer content
-->
</div>
</body>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.mousewheel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/owl/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
</html>
